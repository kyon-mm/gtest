import net.sourceforge.plantuml.SourceStringReader
String imagePath="./build/sampleImage.png"
String textPath="./build/sampleText.txt"
File imageFile = new File(imagePath)
def text = (["@startuml"] + new File(textPath).readLines() + ["@enduml"]).unique().join("\n")
println text
def s = new SourceStringReader(text)
s.generateImage(imageFile)
imageFile
