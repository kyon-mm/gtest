package org.kyonmm.piece

/**
 *
 */
class Constraint {
    String name
    Closure condition

    def and(Constraint constraint) {
        def c = {
            this.condition(it) && constraint.condition(it)
        }
        new Constraint(name: this.name + " & " + constraint.name, condition: c)
    }

    static Constraint constraint(String name, Closure condition) {
        new Constraint(name: name, condition: condition)
    }
}
