package org.kyonmm.piece

import groovy.transform.Canonical

/**
 * Created with IntelliJ IDEA.
 * User: 小林
 * Date: 13/12/11
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */
@Canonical
class Parameter{
    String name
    List value
    Closure validate
    def modify(Closure closure) {
        new Parameter(name:this.name, value: value.collect{closure.call(it)}, validate: validate)
    }
}
