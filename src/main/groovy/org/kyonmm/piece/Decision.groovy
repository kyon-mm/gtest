package org.kyonmm.piece

import org.junit.runner.RunWith
import org.kyonmm.internals.decision.*
import org.kyonmm.internals.runner.GTestRunner

class ListEx {
    public static <S extends Piece> SpecifiedDecision rightShiftUnsigned(Map self, Class<S> test) {
        def result = new SpecifiedDecision(test)
        self.put("constraintName", self.toString())
        result.params = self.asType(getPreConditionType(test))
        result
    }

    public static Class getPreConditionType(Class test) {
        if (test.superclass == Piece) {
            test.genericSuperclass.actualTypeArguments[0]
        } else {
            getPreConditionType(test.superclass)
        }
    }
}


@RunWith(GTestRunner)
abstract class Decision {
    List<TestCaseFactory> test() {
        use(ListEx) {
            testCases().flatten()
        }
    }

    abstract List<TestCaseFactory> testCases()

    public <U extends PreCondition> DecisionRow All(Class<U> condition, Parameter... parameters) {
        parameters.each {
            assert it.validate?.call() ?: true, "水準が性質を満たしていません。${it.value}"
        }
        U conditionInstance = condition.newInstance()
        def constraintNames = []
        conditionInstance.validPropertyNames().eachWithIndex{propertyName, i -> constraintNames += propertyName + "=" + parameters[i].value}
        new DecisionRow(preConditions: parameters.collect { it.value }.combinations().collect {List values ->
            U result = condition.newInstance(* values)
            result.constraintName = constraintNames.join(",")
            result
        })
    }

    public <U extends Piece> ListDecision All(Class<U> piece) {
        Decision before = piece.declaringClass.newInstance()
        new ListDecision(factories: before.testCases().findAll { it.piece == piece})
    }
    public <U extends Piece> ListDecision All(Class decision, Class<U> piece) {
        Decision before = decision.newInstance()
        new ListDecision(factories: before.testCases().findAll { it.piece == piece })
    }

    public StubDecision Stub(State state) {
        state.setConstraintName()
        new StubDecision(state: state)
    }

    public <T extends State>List<StubDecision> StubAll(Class<T> stateClass, Parameter... parameters) {
        parameters.each {
            assert it.validate?.call() ?: true, "水準が性質を満たしていません。${it.value}"
        }
//        T stateInstance = stateClass.newInstance()
//        def constraintNames = []
//        stateInstance.validPropertyNames().eachWithIndex{propertyName, i -> constraintNames += propertyName + "=" + parameters[i].value}
        parameters.collect { it.value }.combinations().collect {
            State state = stateClass.newInstance(*it)
            state.setConstraintName(parameters.collect{it.value})
            new StubDecision(state: state)
        }
    }

    public <U extends PreCondition> List<U> Once(Class<U> condition, Parameter... parameters) {
        All(condition, parameters)
    }
    public Branch trunk(Constraint constraint) {
        Tree.trunk(constraint)
    }

}
