package org.kyonmm.piece

import org.kyonmm.internals.decision.TestCase
import org.kyonmm.internals.decision.TestCaseFactory

/**
 *
 */
class Tree{
    List<Branch> branches = []
    static trunk(Constraint constraint){
        Tree tree = new Tree()
        Branch branch = new Branch(constraint: constraint, tree: tree)
        tree.branches += branch
        branch
    }
}
class Branch {
    Tree tree
    Constraint constraint
    Class<Piece> piece
    public Branch leaf(Class<Piece> piece){
        this.piece = piece
        Branch branch = new Branch(tree: tree)
        tree.branches += branch
        branch
    }
    public Branch branch(Constraint constraint){
        this.constraint = constraint
        this
    }
    public Tree other(Class<Piece> piece){
        this.piece = piece
        tree
    }
}
