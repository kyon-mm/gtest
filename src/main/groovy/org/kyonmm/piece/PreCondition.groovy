package org.kyonmm.piece

import org.kyonmm.internals.decision.TestCase

/**
 * Created with IntelliJ IDEA.
 * User: 小林
 * Date: 13/12/11
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
class PreCondition<S extends PreCondition<S>> {

    String constraintName
    List<TestCase> beforeTestCase

    String toString(){
        this.metaClass.properties.findAll{["class", "constraintName", "beforeTestCase"].contains(it.name) == false}.sort{it.name}.collect{it.name + "=" + it.getProperty(this)}.join(', ')
    }
    List<String> validPropertyNames(){
        this.metaClass.properties.findAll{["class", "constraintName", "beforeTestCase"].contains(it.name) == false}.collect{it.name}
    }
}
class EmptyPreCondition extends PreCondition{

}
