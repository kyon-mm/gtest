package org.kyonmm.piece

import sun.reflect.generics.reflectiveObjects.NotImplementedException

/**
 * Created with IntelliJ IDEA.
 * User: 小林
 * Date: 13/12/11
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */
abstract class Piece<T extends PreCondition, U extends State> {
    String name = ""
    private String description = ""
    public U state

    abstract U sunny(T parameter)

    abstract U invariant(T parameter)

    abstract U prepare(T parameter)

    public U rainy(T parameter) { State.EMPTY }

    public U misUseCase(T parameter, Exception ex) { throw ex }

    protected sunnyAct(T parameter, U expected) {
        if (expected == null) {
            sunnyAction(parameter)
        } else {
            sunnyAction(parameter, expected)
        }

    }

    abstract boolean sunnyAction(T parameter)
    U unitState(){state}

    boolean sunnyAction(T parameter, U expected) {
        if (expected == null) {
            sunnyAction(parameter)
        } else {
            return false
        }
    }

    protected rainyAct(T parameter, U expected) {
        if (expected == null) {
            rainyAction(parameter)
        } else {
            rainyAction(parameter, expected)
        }
    }

    public boolean rainyAction(T parameter) {
        return false
    }

    U pipe(state, parameter){
        throw new NotImplementedException()
    }

    public boolean rainyAction(T parameter, U expected) {
        return false
    }

    void given(String args) {
        description += args
    }

    void when(String args) {
        description += args

    }

    void then(String args) {
        description += args
    }
    public run = { T parameter, beforeTest, U expected = null ->
        state = unitState()
        try {
            if(beforeTest == null || beforeTest.class == PieceEmpty){
                state = prepare(parameter)
            }
            else{
                state = pipe(beforeTest.run.call(), parameter)
            }
            invariant(parameter)
            state = sunny(parameter)
            sunnyAct(parameter, expected)
            invariant(parameter)
        }
        catch (AssertionError ae) {
            if (!isRainyActionOverride(parameter, expected)) {
                throw ae;
            }
            state = rainy(parameter)
            rainyAct(parameter, expected)
        }
        catch (Exception ex) {
            state = misUseCase(parameter, ex)
        }
        state
    }

    boolean isRainyActionOverride(T parameter, U expected) {
        def targetParameterTypes = expected == null ? [parameter.class] : [parameter.class, expected.class]
        this.metaClass.methods.findAll {
            it.name == "rainyAction"
        }.findAll{it.declaringClass.name != Piece.name}.findAll{it.nativeParameterTypes == targetParameterTypes}
    }

    static class PieceEmpty extends Piece {
        {
            name = "から"
        }

        @Override
        State sunny(PreCondition parameter) {
            return null  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        State invariant(PreCondition parameter) {
            return null  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        State prepare(PreCondition parameter) {
            return null  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        boolean sunnyAction(PreCondition parameter) {
            return false  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        State unitState() {
            return null
        }
    }

    static class StubPiece extends Piece {
        {
            name = "スタブ"
        }

        @Override
        State sunny(PreCondition parameter) {
            state
        }

        @Override
        State invariant(PreCondition parameter) {
            state
        }

        @Override
        State prepare(PreCondition parameter) {
            state
        }

        @Override
        boolean sunnyAction(PreCondition parameter) {
            state
        }

        @Override
        State unitState() {
            return null
        }
    }
}
