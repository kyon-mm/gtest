package org.kyonmm.piece

/**
 * Created with IntelliJ IDEA.
 * User: 小林
 * Date: 13/12/11
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
class State{
    String name
    State before
    String constraintName
    static final State EMPTY = new State()
    void setConstraintName(){
        constraintName = validProperties().join(",")
    }
    void setConstraintName(List values){
        int i = 0
        constraintName = validPropertyNames().collect{it + "=" + values[i++]}.join(",")
    }
    String toString(){
        (name?: "") + "=" + constraintName
    }
    List<String> validProperties(){
        this.metaClass.properties.findAll{["class", "constraintName", "before", "name", "EMPTY"].contains(it.name) == false}.collect{it.name + "=" + it.getProperty(this)}
    }
    List<String> validPropertyNames(){
        this.metaClass.properties.findAll{["class", "constraintName", "before", "name", "EMPTY"].contains(it.name) == false}.collect{it.name}
    }
}
