package org.kyonmm.arrt

import org.junit.runner.RunWith
import org.kyonmm.internals.decision.TestCaseFactory
import org.kyonmm.internals.runner.GTestRunner
import org.kyonmm.piece.Decision

/**
 *
 */
@RunWith(GTestRunner)
class BackLog {
    List<Class> decisions = []
    List<Class> ignored = []
    List<Risk> risks = []

    void DOING(Class clazz) {
        if (isDecisionSubClass(clazz) == false) {
            throw IllegalArgumentException("clazz must be Decision. but clazz is ${clazz}")
        }
        decisions += clazz
    }

    void DONE(Class clazz) {
        DOING(clazz)
    }

    void TODO(Class clazz) {
        if (isDecisionSubClass(clazz) == false) {
            throw IllegalArgumentException("clazz must be Decision. but clazz is ${clazz}")
        }
        ignored += clazz
    }

    boolean isDecisionSubClass(Class clazz) {
        if (clazz?.superclass == Object) {
            false
        } else if (clazz?.superclass == Decision) {
            true
        } else {
            isDecisionSubClass(clazz.superclass)
        }
    }

    List<TestCaseFactory> test() {
        decisions.collect { it.newInstance().test() }.flatten()
    }

    Risk これらは(PositiveAnswer positiveAnswer, NegativeAnswer negativeAnswer) {

    }
    BackLog 本スプリント = this
}
