package org.kyonmm.arrt

/**
 *
 */
class Answer {
    int point
}
class PositiveAnswer extends Answer{
    static final PositiveAnswer 解決されてうれしいし = [point:1]
    static final PositiveAnswer 解決されて当然であるし = [point:2]
    static final PositiveAnswer 解決されても何とも思わないし = [point:3]
    static final PositiveAnswer 解決されても構わない = [point:4]
    static final PositiveAnswer 解決されたら困る = [point:5]
}
class NegativeAnswer extends Answer{
    static final NegativeAnswer 解決されなくてうれしいし = [point:1]
    static final NegativeAnswer 解決されなくて当然であるし = [point:2]
    static final NegativeAnswer 解決されなくて何とも思わないし = [point:3]
    static final NegativeAnswer 解決されなくて構わない = [point:4]
    static final NegativeAnswer 解決されなかったら困る = [point:5]
}