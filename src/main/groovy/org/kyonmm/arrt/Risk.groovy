package org.kyonmm.arrt

/**
 *
 */
class Risk {
    List<Risk> dependencies = []
    String name
    Class decision
    BackLog backLog

    Risk dependsOn(List<Risk> dependencies) {
        this.dependencies += dependencies
        this
    }

    Risk リスクを軽減するために(Class clazz) {
        decision = clazz
        this
    }

    Risk を(backLog) {
        this.backLog = backLog
        this
    }

    Risk で実装する(Estimate) {
        backLog.DOING(decision)
        this
    }

    Risk で計画している(Estimate) {
        backLog.TODO(decision)
        this
    }

    Risk で完了した(Estimate) {
        backLog.DONE(decision)
        this
    }
}
