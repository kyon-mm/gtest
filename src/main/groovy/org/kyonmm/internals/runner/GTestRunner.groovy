package org.kyonmm.internals.runner

import org.junit.runner.Description
import org.junit.runner.Runner
import org.junit.runner.notification.Failure
import org.junit.runner.notification.RunNotifier
import org.kyonmm.internals.decision.TestCase
import org.kyonmm.internals.decision.TestCaseFactory
import org.kyonmm.internals.documenter.UmlState
import org.kyonmm.piece.Decision
import org.kyonmm.piece.Piece

class GTestRunner extends Runner {
    Class<?> clazz
    List<TestCaseFactory> factories = []
    List<TestCase> testCases = []
    private static final String ARROW = " -> "
    private static final String STATE_ARROW = " --> "
    private static final String STATE_INIT = "[*]"
    private static final def brace = /{[.+]}/

    GTestRunner(Class<?> clazz) {
        this.clazz = clazz
        def instance = clazz.newInstance()
        if (instance instanceof Decision) {
            factories = instance.test()
        } else {
            factories = clazz.newInstance()."test"()
        }
        testCases = factories.collect{
            it.buildTestCase().combinations().collect{List<TestCase> t ->
                t.inject {TestCase beforeCase, TestCase currentCase ->
                    Piece currentTestInstance = currentCase.piece.class.newInstance()
                    currentTestInstance.run = currentTestInstance.run.curry(currentCase.parameter, beforeCase?.piece?.run, currentCase.expected)
                    def description = "${addArrow(beforeCase.description)} ${currentTestInstance.name}(${currentCase.parameter.constraintName}${currentCase.parameter.constraintName ?: ""})"
                    new TestCase(piece: currentTestInstance, description: description, parameter: currentCase.parameter)
                }
            }
        }.flatten()
    }


    static addArrow(str) {
        if (str) {
            str + ARROW
        } else {
            ""
        }
    }
    static String replaceStateArrow(String str){
        def result = str
        if(str.contains(ARROW) == false || str.startsWith("スタブ")){
            result = STATE_INIT + STATE_ARROW + str
        }
        result.replaceAll(ARROW, STATE_ARROW)
    }
    static String replaceComment(String str){
        def text = str.replaceAll(/\:/, "=")
        (text =~ /([^\(\)]+)\(([^\(\)]+)\)/).collect{all, a , b->
            a + ":" + b
        }.join("")
    }
    static String changeArrowLine(String str){
        def result = []
        str.split(STATE_ARROW).inject(""){p, n ->
            if(p == "") return n
            result += p + STATE_ARROW + n
            n.replaceAll(/:.+/, "")
        }
        result.join("\n")
    }
    static String toPlantStateStyle(String str){
        changeArrowLine(replaceComment(replaceStateArrow(str)))
    }

    @Override
    Description getDescription() {
        def desc = Description.createSuiteDescription(clazz.name)
        def descChild
        testCases.eachWithIndex { testCase, i ->
            if(i == 0){
                descChild = Description.createSuiteDescription(testCase.piece.class)
                desc.addChild(descChild)
            }
            def currentDescription = Description.createTestDescription(testCase.piece.class, testCase.description)
            descChild.addChild(currentDescription)
        }
        desc
    }

    @Override
    void run(RunNotifier notifier) {
        def desc = getDescription()
        int i = 0
        def umlState = UmlState.instance
        desc.getChildren().each { child ->
            child.getChildren().each{currentDescription ->
                notifier.fireTestStarted(currentDescription)
                try {
                    def currentTestCase = testCases[i++]
                    umlState.writeState(toPlantStateStyle(currentTestCase.description))
                    currentTestCase.run()
                }
                catch (Throwable e) {
                    notifier.fireTestFailure(new Failure(currentDescription, e))
                }
                notifier.fireTestFinished(currentDescription)
            }
        }
    }

}
