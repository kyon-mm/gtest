package org.kyonmm.internals.documenter

import net.sourceforge.plantuml.SourceStringReader

/**
 *
 */
@Singleton
class UmlState {
    String imagePath
    String textPath
    File imageFile
    File textFile
    boolean outputGraph
    {
        ConfigObject config = new ConfigSlurper().parse(new File("GraceConfig.groovy").toURI().toURL())
        imagePath = config.imagePath
        textPath = config.textPath
        textFile = new File(textPath)
        imageFile = new File(imagePath)
        textFile.createNewFile()
        imageFile.createNewFile()
        outputGraph = config.outputGraph
    }
    File generateImage(){
        if(outputGraph == false){
            imageFile
        }
        def text = (["@startuml"] + new File(textPath).readLines() + ["@enduml"]).unique().join("\n")
        def s = new SourceStringReader(text)
        s.generateImage(imageFile)
        imageFile
    }
    void writeState(String action){
        textFile.append(action + "\n", "utf-8")
    }
    void clearFile(){
        textFile.deleteOnExit()
        textFile.createNewFile()
        imageFile.deleteOnExit()
        imageFile.createNewFile()
    }
}
