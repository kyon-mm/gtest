package org.kyonmm.internals.decision

import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition

/**
 *
 */
class ListDecision extends TestCaseFactory {

    List<TestCaseFactory> factories = []
    List<ListDecision> decisions = []

    @Override
    def <U extends PreCondition> List<List<TestCase<U>>> buildTestCase() {
        ([factories.collect { it.buildTestCase() }.flatten()] + decisions.collect {
            it.buildTestCase().flatten()
        }).findAll { it.size() != 0 }
    }

    public <S extends Piece> ListDecision rightShiftUnsigned(ListDecision afterTest) {
        this.decisions.addAll(afterTest)
        this
    }
}
