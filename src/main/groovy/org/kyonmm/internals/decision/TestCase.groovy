package org.kyonmm.internals.decision

import groovy.transform.Canonical
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition

/**
 *
 */
@Canonical
class TestCase<U extends PreCondition> {
    String description
    Piece piece = new Piece.PieceEmpty()
    U parameter
    def expected

    def run() {
        piece.run.call()
    }

    static final TestCase EMPTY_CASE = new TestCase(description: "")
}
