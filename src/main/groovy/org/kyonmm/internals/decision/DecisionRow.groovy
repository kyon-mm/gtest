package org.kyonmm.internals.decision

import groovy.transform.AutoClone
import org.kyonmm.piece.Constraint
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.Tree

/**
 *
 */
@AutoClone
class DecisionRow extends TestCaseFactory {
    List preConditions
    List<TestCaseFactory> before = []
    List<TestCaseFactory> after = []
    Constraint constraint

    @Override
    def <U extends PreCondition> List<List<TestCase<U>>> buildTestCase() {
        (before.collect { it.collect { it.buildTestCase() } }.collect {
            it.flatten()
        } + createMainTestCase() + [after.collect { it.buildTestCase() }.flatten()]).findAll { it.size() != 0 }
    }

    private <U extends PreCondition> List<List<TestCase<U>>> createMainTestCase() {
        [preConditions.collect { params ->
            Piece currentTestInstance = piece.newInstance()
            currentTestInstance.run = currentTestInstance.run.curry(params, null, null)
            new TestCase(description: "${currentTestInstance.name}(${params.constraintName})",
                    piece: currentTestInstance,
                    parameter: params,
                    expected: null)
        }]
    }


    public <S extends Piece> DecisionRow rightShiftUnsigned(Class<S> piece) {
        this.piece = piece
        this
    }

    public <S extends Piece> List<DecisionRow> rightShiftUnsigned(Tree tree) {
        tree.branches.collect{branch ->
            DecisionRow result = this.clone()
            result.piece = branch.piece
            result.constraint = branch.constraint
            def targetPreConditions = preConditions.findAll {branch.constraint == null ? true : branch.constraint.condition.call(it)}
            preConditions -= targetPreConditions
            result.preConditions = targetPreConditions
            result
        }
    }

    public <S extends Piece> DecisionRow rightShiftUnsigned(Constraint constraint) {
        preConditions = preConditions.findAll { PreCondition preCondition ->
            constraint.condition.call(preCondition)
        }
        this
    }

    public DecisionRow leftShift(List<TestCaseFactory> before) {
        this.before.add(0, before)
        this
    }

    public DecisionRow leftShift(TestCaseFactory before) {
        this.before.add(0, before)
        this
    }

    public <S extends Piece> DecisionRow rightShiftUnsigned(List<TestCaseFactory> after) {
        this.after.addAll(after)
        this
    }
}
