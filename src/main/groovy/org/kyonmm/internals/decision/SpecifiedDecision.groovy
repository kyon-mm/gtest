package org.kyonmm.internals.decision

import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition

/**
 *
 */
class SpecifiedDecision extends TestCaseFactory {

    def params

    SpecifiedDecision(Class piece) {
        this.piece = piece
    }

    @Override
    def <U extends PreCondition> List<List<TestCase<U>>> buildTestCase() {
        Piece currentTestInstance = piece.newInstance()
        currentTestInstance.run = currentTestInstance.run.curry(params, null, expected)
        [[new TestCase(description: "${currentTestInstance.name}(${params.constraintName})",
                piece: currentTestInstance,
                parameter: params,
                expected: expected)]]
    }

    public <S extends Piece> SpecifiedDecision rightShiftUnsigned(Map expected) {
        Class ex = getStateType(piece)
        this.expected = expected.asType(ex)
        this
    }

    public Class getStateType(Class test) {
        if (test.superclass == Piece) {
            test.genericSuperclass.actualTypeArguments[1]
        } else {
            getStateType(test.superclass)
        }
    }
}
