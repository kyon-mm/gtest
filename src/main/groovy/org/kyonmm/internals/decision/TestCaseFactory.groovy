package org.kyonmm.internals.decision

import org.kyonmm.piece.PreCondition

/**
 *
 */
abstract class TestCaseFactory {
    Class piece
    def expected

    public abstract <U extends PreCondition> List<List<TestCase<U>>> buildTestCase()
}
