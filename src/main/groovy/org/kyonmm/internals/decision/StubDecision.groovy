package org.kyonmm.internals.decision

import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State

/**
 *
 */
class StubDecision extends TestCaseFactory {
    State state
    List<StubDecision> before

    @Override
    def <U extends PreCondition> List<List<TestCase<U>>> buildTestCase() {
        Piece testInstance = new Piece.StubPiece(state: state)
        [[new TestCase(piece: testInstance, description: "${testInstance.name}(${state.toString()})")]]
    }

    public StubDecision leftShift(StubDecision before) {
        this.before.add(0, before)
        this
    }
}
