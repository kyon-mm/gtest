package org.kyonmm.unittest

import spock.lang.Specification
import spock.lang.Unroll

/**
 *
 */
class ListDecisionSpec extends Specification {
    @Unroll
    def "All piece buildTestCase -> List<TestCase>"() {
        when:
        def testCases = new DecisionSpec.Sample().All(DecisionSpec.Sample.SamplePiece).buildTestCase()
        then:
        assert testCases.size() > 0
        testCases.each{
            assert it.size() > 0
        }
    }
}