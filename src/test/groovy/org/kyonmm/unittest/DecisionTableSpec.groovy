package org.kyonmm.unittest

import org.kyonmm.internals.decision.DecisionRow
import spock.lang.Specification
import spock.lang.Unroll

/**
 *
 */
class DecisionTableSpec extends Specification {
    @Unroll
    def "DecisionTable >>> Piece -> DecisionTable"() {
        expect:
        def actual = decision >>> piece
        actual.class == DecisionRow
        where:
        decision            | piece
        new DecisionRow() | DecisionSpec.Sample
    }

    @Unroll
    def "All condition parameter >>> Piece -> buildTestCase -> DecisionTable"() {
        expect:
        DecisionRow decisionTable = new DecisionSpec.Sample().All(condition, str1, int1) >>> piece
        def actual = decisionTable.buildTestCase()
        assert actual.size() > 0
        actual.each{
            it.each{
                assert str1.value.contains(it.parameter.str1)
                assert int1.value.contains(it.parameter.int1)
            }
        }
        where:
        condition                    | str1          | int1    | piece
        DecisionSpec.SampleCondition | DecisionSpec.Japenese.shortJapanese | DecisionSpec.NaturalNumber._1Digit | DecisionSpec.Sample.SamplePiece
    }
}