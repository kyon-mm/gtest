package org.kyonmm.unittest

import groovy.transform.TupleConstructor
import org.kyonmm.piece.Decision
import org.kyonmm.internals.decision.DecisionRow
import org.kyonmm.internals.decision.ListDecision
import org.kyonmm.piece.Parameter
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State
import spock.lang.Specification
import spock.lang.Unroll

import static DecisionSpec.Japenese.*
import static DecisionSpec.NaturalNumber.*

/**
 *
 */
class DecisionSpec extends Specification {

    static class Sample extends Decision {
        @Override
        List testCases() {
            [
                    All(SampleCondition, shortJapanese, _1Digit) >>> SamplePiece
            ]
        }
        static class SamplePiece    extends Piece<SampleCondition, SampleState> {
            {
                state = new SampleState()
            }
            @Override
            SampleState sunny(SampleCondition parameter) {
                state
            }

            @Override
            SampleState invariant(SampleCondition parameter) {
                state
            }

            @Override
            SampleState prepare(SampleCondition parameter) {
                state
            }

            @Override
            boolean sunnyAction(SampleCondition parameter) {
                state
            }
        }
    }


    static class SampleState extends State {
        int result
    }

    @TupleConstructor
    static class SampleCondition extends PreCondition<SampleCondition> {
        String str1
        int int1
    }

    class Japenese extends Parameter {
        static final Parameter shortJapanese = [name: "短い日本語", value: ["あ", "さんぷる"]]
    }

    class NaturalNumber extends Parameter {
        static final Parameter _1Digit = [name: "1桁", value: [1, 3, 5]]
    }

    @Unroll
    def "All Condition paramter -> DecisionTable"() {
        expect:
        def actual = new Sample().All(SampleCondition, str1, int1)
        actual.class == DecisionRow
        where:
        str1          | int1
        shortJapanese | _1Digit
    }
    @Unroll
    def "All Piece -> DecisionTable"() {
        expect:
        def actual = new Sample().All(Sample.SamplePiece)
        actual.class == ListDecision
    }

    def "All Piece -> List<TestCaseFactory>"() {
        when:
        ListDecision actual = new Sample().All(Sample, Sample.SamplePiece)
        then:
        actual.factories.size() > 0
        actual.factories.every{
            it.piece == Sample.SamplePiece
        }
    }
    def "All Piece >>> All Piece -> List<TestCaseFactory>"() {
        when:
        ListDecision actual = new Sample().All(Sample, Sample.SamplePiece) >>> new Sample().All(Sample, Sample.SamplePiece)
        then:
        actual.factories.size() > 0
        actual.factories.every{
            it.piece == Sample.SamplePiece
        }
    }
}