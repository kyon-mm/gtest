package org.kyonmm.unittest

import org.kyonmm.piece.ListEx
import org.kyonmm.internals.decision.SpecifiedDecision
import org.kyonmm.internals.decision.TestCase
import spock.lang.Specification
import spock.lang.Unroll


/**
 *
 */
class SpecifiedDecisionSpec extends Specification {

    @Unroll
    def "parameter >>> test -> SpecifiedDecision"() {
        use(ListEx) {
            when:
            SpecifiedDecision actual = (parameter >>> test)
            then:
            assert actual.piece == test
            assert actual.params.foo1 == parameter.foo1
            assert actual.params.foo2 == parameter.foo2
        }
        where:
        parameter           | test
        [foo1: 10, foo2: 2] | DivideTest.Divide
    }

    @Unroll
    def "SpecifiedDecision >>> expected -> SpecifiedDecision"() {
        use(ListEx) {
            when:
            SpecifiedDecision actual = (decision >>> expected)
            then:
            assert actual.expected.result == expected.result
        }
        where:
        decision                                 | expected
        new SpecifiedDecision(DivideTest.Divide) | [result: 1]
    }

    @Unroll
    def "parameter >>> test >>> expected -> SpecifiedDecision"() {
        use(ListEx) {
            when:
            SpecifiedDecision actual = (parameter >>> test >>> expected)
            then:
            assert actual.piece == test
            assert actual.params.foo1 == parameter.foo1
            assert actual.params.foo2 == parameter.foo2
            assert actual.expected.result == expected.result
        }
        where:
        parameter           | test              | expected
        [foo1: 10, foo2: 2] | DivideTest.Divide | [result: 1]
    }
    @Unroll
    def "SpecifiedDecision buildTestCase -> Single TestCase"() {
        use(ListEx) {
            when:
            SpecifiedDecision decision = (parameter >>> test >>> expected)
            List<List<TestCase>> testCases = decision.buildTestCase()
            then:
            testCases.each{testCase ->
                assert testCase.size() == 1
                assert testCase.head().parameter.foo1 == parameter.foo1
                assert testCase.head().parameter.foo2 == parameter.foo2
            }
        }
        where:
        parameter           | test              | expected
        [foo1: 10, foo2: 2] | DivideTest.Divide | [result: 1]
    }

}