package org.kyonmm.product

import static org.kyonmm.spec.DatabaseSpec.*
import static org.kyonmm.spec.UserSpec.*

/**
 *
 */
class App {
    Database database

    String createUser(String name) {
        if (isValidName(name)) {
            try {
                insertUserToDatabase(name)
                returnSuccessMessage()
            }
            catch (Exception e) {
                printExceptionAndReturnSystemErrorMessage(e)
            }
        } else {
            returnValidErrorMessage()
        }
    }

    User getUserById(int id) {
        try {
            selectUserFromDatabase(id)
        }
        catch (Exception e) {
            printExceptionAndReturnErrorUser(e)
        }
    }

    String editUserName(int id, String name) {
        if (isValidId(id) && isValidName(name)) {
            try {
                editUserNameToDatabase(id, name)
                returnSuccessMessage()
            }
            catch (Exception e) {
                printExceptionAndReturnSystemErrorMessage(e)
            }
        } else {
            returnValidErrorMessage()
        }
    }

    String eraseUser(int id) {
        if (isValidId(id)) {
            try {
                eraseUserFromDatabase(id)
                returnSuccessMessage()
            }
            catch (Exception e) {
                printExceptionAndReturnSystemErrorMessage(e)
            }
        } else {
            returnValidErrorMessage()
        }
    }

    private isValidName(String name) {
        return name != ""
    }

    private isValidId(int id) {
        return id > 0
    }

    private insertUserToDatabase(String name) {
        database.insertUser(name)
    }

    private selectUserFromDatabase(int id) {
        if(! isValidId(id)) return NoUser
        User user = database.selectUserById(id)
        user != null ? user : NoUser
    }

    private editUserNameToDatabase(int id, String name) {
        database.updateUserName(id, name)
    }

    private eraseUserFromDatabase(int id) {
        database.deleteUserById(id)
    }

    private String returnSuccessMessage() {
        return Success
    }

    private String returnValidErrorMessage() {
        return ValidError
    }

    private String printExceptionAndReturnSystemErrorMessage(Exception e) {
        println e
        return SystemError
    }

    private User printExceptionAndReturnErrorUser(Exception e) {
        println e
        return ErrorUser
    }
}
