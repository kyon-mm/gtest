package org.kyonmm.product

/**
 *
 */
class Database {
    List<User> data = []

    void insertUser(String name) {
        try {
            insertOrThrowIllegalArgumentException(name)
        }
        catch (Exception e) {
            throwException(e)
        }
    }

    User selectUserById(int id) {
        try {
            findUser(id)
        }
        catch (Exception e) {
            throwException(e)
        }
    }

    void updateUserName(int id, String name) {
        try {
            updateUserOrThrowIllegalArgumentException(id, name)
        }
        catch (Exception e) {
            throwException(e)
        }
    }

    void deleteUserById(int id) {
        try{
            deleteUserOrThrowIllegalArgumentException(id)
        }
        catch (Exception e){
            throwException(e)
        }
    }

    private int getMaxId() {
        this.data != [] ? this.data.collect { it.id }.max() : 0
    }

    private boolean isExistId(int id) {
        this.data.collect { it.id }.contains(id)
    }

    private insertOrThrowIllegalArgumentException(String name) {
        int id = getNewId()
        isValidParameter(id, name) ? insert(id, name) : throwIllegalArgumentException()
    }

    private int getNewId() {
        def tmpId = maxId + 1
        isExistId(tmpId) ? -1 : tmpId
    }

    private boolean isValidParameter(int id, String name) {
        id > 0 && name != ""
    }

    private void insert(int id, String name) {
        data.add(new User(id: id, name: name))
    }

    private void throwException(Exception e) {
        throw e
    }

    private void throwIllegalArgumentException() {
        throwException(new IllegalArgumentException("IDか名前が不正です"))
    }

    private User findUser(int id) {
        data.find { it.id == id }
    }

    private void updateUserOrThrowIllegalArgumentException(int id, String name) {
        isValidParameter(id, name) && isExistId(id) ? selectUserById(id).name = name : throwIllegalArgumentException()
    }

    private void deleteUserOrThrowIllegalArgumentException(int id) {
        isExistId(id) ? deleteUser(id) : throwException(new IllegalArgumentException("IDが不正です"))
    }

    private deleteUser(int id) {
        data = data.minus(selectUserById(id))
    }
}
