package org.kyonmm

import groovy.transform.Canonical
import groovy.transform.TupleConstructor
import org.kyonmm.piece.Constraint
import org.kyonmm.piece.Decision
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State

class InitTest extends Piece {

    @Override
    State sunny(PreCondition parameter) {
        State.EMPTY
    }

    @Override
    State invariant(PreCondition parameter) {
        return null  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    State prepare(PreCondition parameter) {
        return null  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    boolean sunnyAction(PreCondition parameter) {
        return false  //To change body of implemented methods use File | Settings | File Templates.
    }
}


class Coins {
    int foo1
    int foo2
}


import static org.kyonmm.piece.Constraint.*
import static org.kyonmm.DataSpec.CoinSpecData.*
import static org.kyonmm.MultiplyTest.MultiplyCondition

class Before<T> {
    Class<T> target

    def filter(T t) {

    }
}

class DivideTest extends Decision {
    @TupleConstructor
    static class ClickCondition extends PreCondition<ClickCondition> {
        int foo1
        int foo2
    }

    @Canonical
    static class ClickState extends State {
        double result
    }

    List testCases() {
        [
                All(ClickCondition, NormalCoin, NormalCoin.modify{it * 3}) >>> Divide,
                All(ClickCondition, NormalCoin, NormalCoin) >>> FooConstraint >>> Divide,
                All(ClickCondition, NormalCoin, NormalCoin) >>> (FooConstraint & BarConstraint) >>> Divide,
                [foo1: 10, foo2: 2] >>> Divide >>> [result: 5],
                [foo1: 10, foo2: 1] >>> Divide >>> [result: 10],
                [foo1: 10, foo2: 0] >>> DivideZero >>> [result: 1],
                All(ClickCondition, NormalCoin, Zero) >>> DivideZero,
                All(ClickCondition, NormalCoin, NormalCoinR) >>> trunk(_Zero).leaf(DivideZero)
                        .branch(_1Digit).leaf(Divide)
                        .other(Divide),
//                All(MultiplyTest.Multiply) >>> trunk(_1DigitM).leaf(Divide)
//                        .other(Divide)

        ]
    }
    Constraint FooConstraint = constraint("fooが50以上でfoo2が100") { ClickCondition condition ->
        condition.foo1 >= 50 && condition.foo2 == 100
    }
    Constraint BarConstraint = constraint("fooが100以上でfoo2が100") { ClickCondition condition ->
        condition.foo1 >= 100 && condition.foo2 == 100
    }

    Constraint _Zero = new Constraint(name: "ゼロ", condition: { ClickCondition condition -> condition.foo2 == 0 })
    Constraint _1Digit = new Constraint(name: "1桁", condition: { ClickCondition condition -> condition.foo1.toString().size() == 1 })
    Constraint _1DigitM = new Constraint(name: "1桁", condition: { MultiplyCondition condition -> condition.bar1.toString().size() == 1 })
    Constraint _2Digit = new Constraint(name: "2桁", condition: { ClickCondition condition -> condition.foo1.toString().size() == 2 })


    class Divide extends Piece<ClickCondition, ClickState> {
        {
            name = "割り算のテスト"
        }

        @Override
        ClickState sunny(ClickCondition parameter) {
            when "割り算をする。"
            new ClickState(result: parameter.foo1 / parameter.foo2)
        }

        @Override
        ClickState invariant(ClickCondition parameter) {
            state
        }

        @Override
        ClickState prepare(ClickCondition parameter) {
            state
        }

        @Override
        boolean sunnyAction(ClickCondition parameter) {
            then "掛け算をしてもとに戻る."
            assert ((state.result * parameter.foo2) - (parameter.foo1)).abs() < 0.1
        }

        @Override
        boolean sunnyAction(ClickCondition parameter, ClickState expected) {
            assert state == expected
        }

        @Override
        ClickState unitState() {
            new ClickState()
        }
    }

    class DivideZero extends Divide {
        {
            name = "除算エラー"
        }

        @Override
        ClickState misUseCase(ClickCondition parameter, Exception ex) {
            assert ex.class == ArithmeticException
            state
        }
    }
}
