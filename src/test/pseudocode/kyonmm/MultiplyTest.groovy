package org.kyonmm

import groovy.transform.TupleConstructor
import org.kyonmm.piece.Decision
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State

import static org.kyonmm.DataSpec.CoinSpecData.*

class MultiplyTest extends Decision {
    @Override
    List testCases() {
        [
                All(MultiplyCondition, NormalCoin, NormalCoin) >>> Multiply << Stub(new MultiplyState(result: 2)),
                All(MultiplyCondition, NormalCoin, NormalCoin) >>> Multiply << StubAll(MultiplyState2, NormalCoin, NormalCoin),
                All(MultiplyCondition, NormalCoin, Zero) >>> Multiply << All(DivideTest.DivideZero),
                All(MultiplyCondition, NormalCoin, Zero) >>> Multiply << All(DivideTest.DivideZero) << Stub(new MultiplyState(result: 5)),
                All(DivideTest.DivideZero) >>> All(DivideTest.DivideZero) >>> All(DivideTest.DivideZero),
//                All(DivideTest.DivideZero)
//                        .case(TestCaseConstraint, All(DivideTest.Divide))
//                        .when{}
//                        .then(All(DivideTest.Divide))
//                        .other()
        ]
    }

    @TupleConstructor
    static class MultiplyCondition extends PreCondition<MultiplyCondition> {
        int bar1
        int bar2
    }

    static class MultiplyState extends State {
        {
            name = "掛け算結果"
        }
        int result
    }

    @TupleConstructor
    static class MultiplyState2 extends State {
        {
            name = "2つの値を格納したStateを作る"
        }
        int result1
        int result2
    }

    class Multiply extends Piece<MultiplyCondition, MultiplyState> {
        {
            name = "掛け算のテスト"
        }

        @Override
        MultiplyState sunny(MultiplyCondition parameter) {
            state.result = parameter.bar1 * parameter.bar2
            state
        }

        @Override
        MultiplyState invariant(MultiplyCondition parameter) {
            state
        }

        @Override
        MultiplyState prepare(MultiplyCondition parameter) {
            assert state.before == null
            state
        }

        @Override
        boolean sunnyAction(MultiplyCondition parameter) {
            if(parameter.bar2 == 0){
                assert state.result == 0
            }
            else{
                assert state.result / parameter.bar2 == parameter.bar1
            }
        }

        @Override
        MultiplyState unitState() {
            new MultiplyState()
        }

        @Override
        MultiplyState pipe(DivideTest.ClickState before, MultiplyCondition parameter) {
            state.result = before.result
            state
        }

    }
}
