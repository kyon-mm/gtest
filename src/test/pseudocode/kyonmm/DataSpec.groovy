package org.kyonmm

import org.kyonmm.piece.Parameter

/**
 * Created with IntelliJ IDEA.
 * User: 小林
 * Date: 13/12/17
 * Time: 14:57
 * To change this template use File | Settings | File Templates.
 */
class DataSpec {
    class CoinSpecData {
        static final r = new Random()
        static final Parameter NormalCoin = [value: [10, 50, 100, 500]]
        static final Parameter Zero = [value: [0]]
        static final Parameter NormalCoinR = [value: [r.nextInt(30),r.nextInt(30), r.nextInt(3), r.nextInt(3), 0]]
    }
}
