package org.kyonmm.unit

import groovy.transform.TupleConstructor
import org.kyonmm.piece.Decision
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State
import org.kyonmm.product.Database
import org.kyonmm.product.User

import static org.kyonmm.spec.UserSpec.*
import static org.kyonmm.spec.DatabaseSpec.*

/**
 *
 */
class DatabaseTest extends Decision {
    @Override
    List testCases() {
        [
                All(NameAndDatabaseCondition, UserName, VarietySizeOfData) >>> InsertUser,
                All(NameAndDatabaseCondition, EmptyUserName, VarietySizeOfData) >>> InsertUserError,
                All(IdAndDatabaseCondition, UserId, HaveData) >>> SelectUser,
                All(IdAndDatabaseCondition, UserId, Nothing) >>> SelectUserError,
                All(UserAndDatabaseCondition, UserForUpdate, HaveData) >>> UpdateUserName,
                All(UserAndDatabaseCondition, UserForUpdateErrorByEnptyName, HaveData) >>> UpdateUserNameError,
                All(UserAndDatabaseCondition, UserForUpdateErrorByNotExistId, HaveData) >>> UpdateUserNameError,
                All(IdAndDatabaseCondition, UserId, HaveData) >>> DeleteUser,
                All(IdAndDatabaseCondition, UserId, Nothing) >>> DeleteUserError
        ]
    }

    @TupleConstructor
    static class NameAndDatabaseCondition extends PreCondition<NameAndDatabaseCondition> {
        String name
        Database database
    }

    @TupleConstructor
    static class DatabaseState extends State {
        Database database = new Database()
    }

    class InsertUser extends Piece<NameAndDatabaseCondition, DatabaseState> {
        {
            name = "ユーザ作成"
            state = new DatabaseState()
        }

        @Override
        DatabaseState sunny(NameAndDatabaseCondition parameter) {
            state.database.insertUser(parameter.name)
            state
        }

        @Override
        DatabaseState invariant(NameAndDatabaseCondition parameter) {
            state
        }

        @Override
        DatabaseState prepare(NameAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new DatabaseState(database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(NameAndDatabaseCondition parameter) {
            List<User> currentData = state.database.data
            int currentDataSize = currentData.size()
            assert currentData[currentDataSize - 1].name == parameter.name
            assert currentDataSize == parameter.database.data.size() + 1
        }
    }

    class InsertUserError extends InsertUser {
        {
            name = "ユーザ作成エラー"
        }

        @Override
        DatabaseState misUseCase(NameAndDatabaseCondition parameter, Exception ex) {
            assert ex.class == IllegalArgumentException
            assert state.database.data.size() == parameter.database.data.size()
        }
    }

    @TupleConstructor
    static class UserState extends State {
        int id
        String name
    }

    @TupleConstructor
    static class IdAndDatabaseCondition extends PreCondition<IdAndDatabaseCondition> {
        int id
        Database database
    }


    class SelectUser extends Piece<IdAndDatabaseCondition, UserState> {
        {
            name = "ユーザ検索"
            state = new UserState()
        }

        @Override
        UserState sunny(IdAndDatabaseCondition parameter) {
            User user = parameter.database.selectUserById(parameter.id)
            state = new UserState(id: user.id, name: user.name)
        }

        @Override
        UserState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        UserState prepare(IdAndDatabaseCondition parameter) {
            state = new UserState()
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            assert state.id == parameter.id
            assert state.name != null
        }
    }

    class SelectUserError extends SelectUser {
        {
            name = "不在ユーザ検索エラー"
            state = new UserState()
        }

        @Override
        UserState misUseCase(IdAndDatabaseCondition parameter, Exception ex) {
            assert ex.class == NullPointerException
        }
    }

    @TupleConstructor
    static class UserAndDatabaseCondition extends PreCondition<UserAndDatabaseCondition> {
        User user
        Database database
    }

    class UpdateUserName extends Piece<UserAndDatabaseCondition, DatabaseState> {
        {
            name = "ユーザ名更新"
            state = new DatabaseState()
        }

        @Override
        DatabaseState sunny(UserAndDatabaseCondition parameter) {
            state.database.updateUserName(parameter.user.id, parameter.user.name)
            state
        }

        @Override
        DatabaseState invariant(UserAndDatabaseCondition parameter) {
            state
        }

        @Override
        DatabaseState prepare(UserAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new DatabaseState(database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(UserAndDatabaseCondition parameter) {
            Database currentDatabase = state.database
            currentDatabase.data.collect {
                assert it.id == parameter.user.id ? it.name == parameter.user.name : it.name == parameter.database.selectUserById(it.id).name
            }
            assert currentDatabase.data.size() == parameter.database.data.size()
        }
    }

    class UpdateUserNameError extends UpdateUserName {
        {
            name = "ユーザ名更新エラー"
            state = new DatabaseState()
        }

        @Override
        DatabaseState misUseCase(UserAndDatabaseCondition parameter, Exception ex) {
            assert ex.class == IllegalArgumentException
            assert state.database.data.size() == parameter.database.data.size()
        }
    }

    class DeleteUser extends Piece<IdAndDatabaseCondition, DatabaseState> {
        {
            name = "ユーザ削除"
            state = new DatabaseState()
        }

        @Override
        DatabaseState sunny(IdAndDatabaseCondition parameter) {
            state.database.deleteUserById(parameter.id)
            state
        }

        @Override
        DatabaseState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        DatabaseState prepare(IdAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new DatabaseState(database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            assert state.database.data.size() ==  parameter.database.data.size() - 1
            assert state.database.data.collect {it.id }.contains(parameter.id) == false
        }
    }

    class DeleteUserError extends DeleteUser {
        {
            name = "不在ユーザ削除エラー"
            state = new DatabaseState()
        }

        @Override
        DatabaseState misUseCase(IdAndDatabaseCondition parameter, Exception ex) {
            assert ex.class == IllegalArgumentException
        }
    }
}
