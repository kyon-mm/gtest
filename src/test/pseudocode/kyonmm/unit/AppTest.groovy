package org.kyonmm.unit

import groovy.transform.TupleConstructor
import org.kyonmm.piece.Decision
import org.kyonmm.piece.Piece
import org.kyonmm.piece.PreCondition
import org.kyonmm.piece.State
import org.kyonmm.product.App
import org.kyonmm.product.Database
import org.kyonmm.product.User

import static org.kyonmm.spec.DatabaseSpec.*
import static org.kyonmm.spec.UserSpec.*

/**
 *
 */
class AppTest extends Decision {
    @Override
    List testCases() {
        [
                All(NameAndDatabaseCondition, UserName, VarietySizeOfData) >>> CreateUser,
                All(NameAndDatabaseCondition, EmptyUserName, VarietySizeOfData) >>> CreateUser,
                All(NameAndDatabaseCondition, UserName, VarietySizeOfData) >>> CreateUserSystemError,
                All(IdAndDatabaseCondition, UserId, HaveData) >>> GetUser,
                All(IdAndDatabaseCondition, IllegalUserId, HaveData) >>> GetUser,
                All(IdAndDatabaseCondition, UserId, HaveData) >>> GetUserError,
                All(UserAndDatabaseCondition, UserForUpdate, HaveData) >>> EditUserName,
                All(UserAndDatabaseCondition, UserForUpdateErrorByIllegalUserId, HaveData) >>> EditUserName,
                All(UserAndDatabaseCondition, UserForUpdateErrorByEnptyName, HaveData) >>> EditUserName,
                All(UserAndDatabaseCondition, UserForUpdateErrorByNotExistId, HaveData) >>> EditUserNameError,
                All(UserAndDatabaseCondition, UserForUpdate, HaveData) >>> EditUserNameError,
                All(IdAndDatabaseCondition, UserId, HaveData) >>> EraseUser,
                All(IdAndDatabaseCondition, IllegalUserId, HaveData) >>> EraseUser,
                All(IdAndDatabaseCondition, UserId, Nothing) >>> EraseUserError,
        ]
    }

    @TupleConstructor
    static class NameAndDatabaseCondition extends PreCondition<NameAndDatabaseCondition> {
        String name
        Database database
    }

    static class MessageAndDatabaseState extends State {
        String message
        Database database
    }

    class CreateUser extends Piece<NameAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ作成"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(NameAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            App app = new App(database: currentDatabase)
            state = new MessageAndDatabaseState(message: app.createUser(parameter.name), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState rainy(NameAndDatabaseCondition parameter) {
            sunny(parameter)
        }

        @Override
        MessageAndDatabaseState invariant(NameAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(NameAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(NameAndDatabaseCondition parameter) {
            List<User> currentData = state.database.data
            int currentDataSize = currentData.size()
            if (currentDataSize > 0) {
                assert currentData[currentDataSize - 1].name == parameter.name
            }
            assert currentDataSize == parameter.database.data.size() + 1
            assert state.message == Success
        }

        @Override
        boolean rainyAction(NameAndDatabaseCondition parameter) {
            List<User> currentData = state.database.data
            assert currentData.size() == parameter.database.data.size()
            assert state.message == ValidError
        }
    }

    class CreateUserSystemError extends Piece<NameAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ作成エラー"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(NameAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            // このタイミングでDatabaseにアクセスできなくなったことを想定
            state = new MessageAndDatabaseState(message: new App(database: null).createUser(parameter.name), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState invariant(NameAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(NameAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(NameAndDatabaseCondition parameter) {
            assert state.message == SystemError
        }
    }

    @TupleConstructor
    static class IdAndDatabaseCondition extends PreCondition<IdAndDatabaseCondition> {
        int id
        Database database
    }

    static class UserState extends State {
        User user
    }

    class GetUser extends Piece<IdAndDatabaseCondition, UserState> {
        {
            name = "ユーザ検索"
            state = new UserState()
        }

        @Override
        UserState sunny(IdAndDatabaseCondition parameter) {
            state = new UserState(user: new App(database: parameter.database).getUserById(parameter.id))
            state
        }

        @Override
        UserState rainy(IdAndDatabaseCondition parameter) {
            sunny(parameter)
        }

        @Override
        UserState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        UserState prepare(IdAndDatabaseCondition parameter) {
            state = new UserState()
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            assert state.user.id == parameter.id
            assert state.user.name != NoUserName
        }

        @Override
        boolean rainyAction(IdAndDatabaseCondition parameter) {
            assert state.user == NoUser
        }
    }

    class GetUserError extends Piece<IdAndDatabaseCondition, UserState> {
        {
            name = "ユーザ検索エラー"
            state = new UserState()
        }

        @Override
        UserState sunny(IdAndDatabaseCondition parameter) {
            // このタイミングでDatabaseにアクセスできなくなったことを想定
            state = new UserState(user: new App(database: null).getUserById(parameter.id))
            state
        }

        @Override
        UserState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        UserState prepare(IdAndDatabaseCondition parameter) {
            state = new UserState()
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            assert state.user == ErrorUser
        }
    }

    @TupleConstructor
    static class UserAndDatabaseCondition extends PreCondition<UserAndDatabaseCondition> {
        User user
        Database database
    }

    class EditUserName extends Piece<UserAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ名更新"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(UserAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            App app = new App(database: currentDatabase)
            state = new MessageAndDatabaseState(message: app.editUserName(parameter.user.id, parameter.user.name), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState rainy(UserAndDatabaseCondition parameter) {
            sunny(parameter)
        }

        @Override
        MessageAndDatabaseState invariant(UserAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(UserAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(UserAndDatabaseCondition parameter) {
            Database currentDatabase = state.database
            currentDatabase.data.collect {
                assert it.id == parameter.user.id ? it.name == parameter.user.name : it.name == parameter.database.selectUserById(it.id).name
            }
            assert currentDatabase.data.size() == parameter.database.data.size()
            assert state.message == Success
        }

        @Override
        boolean rainyAction(UserAndDatabaseCondition parameter) {
            Database currentDatabase = state.database
            currentDatabase.data.collect {
                assert it.name == parameter.database.selectUserById(it.id).name
            }
            assert currentDatabase.data.size() == parameter.database.data.size()
            assert state.message == ValidError
        }
    }

    class EditUserNameError extends Piece<UserAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ名更新エラー"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(UserAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            App app = new App(database: null)
            state = new MessageAndDatabaseState(message: app.editUserName(parameter.user.id, parameter.user.name), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState invariant(UserAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(UserAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(UserAndDatabaseCondition parameter) {
            Database currentDatabase = state.database
            currentDatabase.data.collect {
                assert it.name == parameter.database.selectUserById(it.id).name
            }
            assert currentDatabase.data.size() == parameter.database.data.size()
            assert state.message == SystemError
        }
    }

    class EraseUser extends Piece<IdAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ削除"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(IdAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            App app = new App(database: currentDatabase)
            state = new MessageAndDatabaseState(message: app.eraseUser(parameter.id), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState rainy(IdAndDatabaseCondition parametar) {
            sunny(parametar)
        }

        @Override
        MessageAndDatabaseState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(IdAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            assert state.database.data.size() == parameter.database.data.size() - 1
            assert state.database.data.collect { it.id }.contains(parameter.id) == false
            assert state.message == Success
        }

        @Override
        boolean rainyAction(IdAndDatabaseCondition parameter) {
            assert state.database.data.size() == parameter.database.data.size()
            assert state.message == ValidError
        }
    }

    class EraseUserError extends Piece<IdAndDatabaseCondition, MessageAndDatabaseState> {
        {
            name = "ユーザ削除エラー"
            state = new MessageAndDatabaseState()
        }

        @Override
        MessageAndDatabaseState sunny(IdAndDatabaseCondition parameter) {
            def currentDatabase = state.database
            App app = new App(database: null)
            state = new MessageAndDatabaseState(message: app.eraseUser(parameter.id), database: currentDatabase)
        }

        @Override
        MessageAndDatabaseState invariant(IdAndDatabaseCondition parameter) {
            state
        }

        @Override
        MessageAndDatabaseState prepare(IdAndDatabaseCondition parameter) {
            List<User> data = parameter.database.data.clone()
            state = new MessageAndDatabaseState(message: "prepare", database: new Database(data: data))
        }

        @Override
        boolean sunnyAction(IdAndDatabaseCondition parameter) {
            Database currentDatabase = state.database
            currentDatabase.data.collect {
                assert it.name == parameter.database.selectUserById(it.id).name
            }
            assert currentDatabase.data.size() == parameter.database.data.size()
            assert state.message == SystemError
        }
    }
}
