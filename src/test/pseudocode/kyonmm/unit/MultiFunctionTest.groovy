package org.kyonmm.unit

import org.kyonmm.internals.decision.TestCaseFactory
import org.kyonmm.piece.Decision

/**
 *
 */
class MultiFunctionTest extends Decision {
    @Override
    List<TestCaseFactory> testCases() {
        [
                All(AppTest.CreateUser) >>> All(AppTest.GetUser),
                All(AppTest.CreateUser) >>> All(AppTest.EditUserName),
                All(AppTest.CreateUser) >>> All(AppTest.EraseUser),
        ] as List<TestCaseFactory>
    }
}
