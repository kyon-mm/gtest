package org.kyonmm.spec

import org.kyonmm.piece.Parameter
import org.kyonmm.product.Database
import org.kyonmm.product.User

/**
 *
 */
class DatabaseSpec {
    static List<User> NoData = []
    static List<User> OneData = [new User(id: 1, name: "nakajima")]
    static List<User> SomeData = [new User(id: 1, name: "nakajima"), new User(id: 2, name: "kobayashi"), new User(id: 3, name: "hirano"), new User(id:100, name:"miyaszaki"), new User(id: 10000, name :"hamano")]

    static Parameter Nothing = [value: [new Database(data: NoData)]]
    static Parameter HaveData = [value: [new Database(data: SomeData)]]
    static Parameter VarietySizeOfData = [value: [new Database(data: NoData), new Database(data: OneData),new Database(data: SomeData),]]

    static String Success = "Success"
    static String ValidError = "Valid Error"
    static String SystemError = "System Error"

}
