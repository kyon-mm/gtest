package org.kyonmm.spec

import org.kyonmm.piece.Parameter
import org.kyonmm.product.User

/**
 *
 */
class UserSpec {
    static int NoUserId = 0
    static String NoUserName = "No User"
    static User NoUser = new User(id: NoUserId, name: NoUserName)

    static int ErrorUserId = -1
    static String ErrorUserName = "Error User"
    static User ErrorUser = new User(id: ErrorUserId, name: ErrorUserName)

    static Parameter IllegalUserId = [value: [-1, 0]]
    static Parameter UserId = [value: [1, 2, 3, 100, 10000]]

    static Parameter EmptyUserName = [value: [""]]
    static Parameter UserName = [value: ["rika", "kyon"]]

    static Parameter UserForUpdate = [value: [new User(id: 1, name: "rika"), new User(id: 2, name: "kyon")]]
    static Parameter UserForUpdateErrorByIllegalUserId = [value: [new User(id: 0, name: "rika"), new User(id: -1, name: "kyon")]]
    static Parameter UserForUpdateErrorByEnptyName = [value: [new User(id: 1, name: ""), new User(id: 2, name: "")]]
    static Parameter UserForUpdateErrorByNotExistId = [value: [new User(id: 5, name: "aaa"), new User(id: 500, name: "bbb")]]
}
